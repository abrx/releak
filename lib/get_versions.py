#!/usr/bin/env python
import logging
import logging.config
import os
import re

import feedparser
import yaml

from lib.nist_cve import NistCVEs

# Kind of an RSS manager specialized on releases, version following and CVE

#####################################################################################
#                                                                                   #
#   Configuration                                                                   #
#                                                                                   #
#####################################################################################


def configure_logging(config_file="config/conf_logging.yaml"):
    """
    Configure logging from yaml config file.
    Log level is set to INFO and can be override by LOGLEVEL environment variable
    """
    with open(config_file, mode="rt", encoding="utf-8") as logging_configuration:
        logging.config.dictConfig(yaml.safe_load(logging_configuration.read()))


def load_inventory(config_file="config/inventory.yaml"):
    """
    Get list of sources / repositories to watch

    YAML structure:

    default:
      github:
        grafana:
          grafana: v10.3.7
        traefik:
          traefik: v3.0.2

    another-app:
      github:
        grafana:
          grafana: v11.0.1
        boostorg:
          boost: boost-1.85.0
        gitlabhq:
          gitlabhq: v16.11.6
    """
    logger = logging.getLogger("releak")
    logger.debug("Get config file: %s", config_file)

    with open(config_file, encoding="utf-8") as conf:
        try:
            return yaml.load(conf, Loader=yaml.FullLoader)
        except yaml.YAMLError as e:
            logger.exception(
                "Error loading configuration from file '%s': %s", config_file, e
            )
            return {}


#####################################################################################
#                                                                                   #
#   Functions                                                                       #
#                                                                                   #
#####################################################################################


def get_repos_users_hash_from_inventory(application="default", provider="github"):
    """
    Return dictionnary of repo:user from this provider
    """
    repo_user_hash = {}
    for user in load_inventory()[application][provider]:
        for repo in list(load_inventory()[application][provider][user]):
            repo_user_hash[repo] = user

    return repo_user_hash


def build_github_url(owner, repo):
    """
    Return release url for github
    """
    return f"https://github.com/{owner}/{repo}/releases.atom"


def get_raw_feed_from_url(url):
    """
    Get raw feed from passed rss url
    """
    logger = logging.getLogger("releak")

    try:
        return feedparser.parse(url)
    except Exception as e:
        logger.exception("Error getting feed from url '%s': %s", url, e)
        return feedparser.util.FeedParserDict()


#####################################################################################
#                                                                                   #
#   Main                                                                            #
#                                                                                   #
#####################################################################################


def main():
    """
    Get yaml config
    Build github urls
    Return feed titles
    """
    # Get logging configuration
    configure_logging()
    # Create logger object and override LOG LEVEL with environment
    logger = logging.getLogger("releak")
    logger.setLevel(os.environ.get("LOGLEVEL", "INFO"))

    cveEngine = NistCVEs()

    # Build url:
    for app in list(load_inventory()):

        print("Application: ", app)
        for repo, user in get_repos_users_hash_from_inventory(
            application=app, provider="github"
        ).items():
            feed_url = build_github_url(user, repo)
            print(f"\n{user}@{repo} :")

            # Return feed list from given repo :
            raw = get_raw_feed_from_url(feed_url)

            # raw has both general feed information and release details under entries
            for release in raw.entries:
                print(f"\t+ {release.title}\t {release.updated}\t {release.link}")

            # Find all related cves
            known_cves = cveEngine.search(repo)
            re_version = re.compile(r"(\d\.[\d\.x]+)")
            if known_cves != {}:
                print(f'++ Overall known CVEs : {known_cves["totalResults"]}')
                for v in known_cves["vulnerabilities"]:
                    print(
                        f'\tid:{v["cve"]["id"]}'
                        f'\tdate:{v["cve"]["published"]}'
                        # hopefully this is okay as the en description always comes first
                        f'\taffects:{re_version.findall(v["cve"]["descriptions"][0]["value"])}'
                    )


if __name__ == "__main__":
    main()
