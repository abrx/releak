import os
from flask import Flask, render_template, send_from_directory
from lib.get_versions import (
    load_inventory,
    get_repos_users_hash_from_inventory,
    build_github_url,
    get_raw_feed_from_url,
)

app = Flask(__name__)


@app.route("/")
def homepage():
    """Home page."""
    return "This is a test."


@app.route("/favicon.ico")
def favicon():
    return send_from_directory(
        os.path.join(app.root_path, "static"),
        "favicon.ico",
        mimetype="image/vnd.microsoft.icon",
    )


@app.route("/releak")
def versions():
    """Where the tags are rendered from templates folder."""
    feed_urls = {}

    # repo is unique but a user can have multiples repos so we use repos as key
    repos_and_users = {}

    all_versions = {}

    for user_app in list(load_inventory()):

        for repo, user in get_repos_users_hash_from_inventory(
            application=user_app, provider="github"
        ).items():
            url = build_github_url(user, repo)
            feed_urls[repo] = url

            repos_and_users[repo] = user

            all_versions[url] = get_raw_feed_from_url(url).entries

    return render_template(
        "releak.html",
        feed_urls=feed_urls,
        all_versions=all_versions,
        repos_and_users=repos_and_users,
        app_list=list(load_inventory()),
    )


if __name__ == "__main__":
    app.run()
